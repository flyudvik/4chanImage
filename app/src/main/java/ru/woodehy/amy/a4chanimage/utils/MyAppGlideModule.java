package ru.woodehy.amy.a4chanimage.utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;


@GlideModule
public final class MyAppGlideModule extends AppGlideModule {}
