package ru.woodehy.amy.a4chanimage.api;


import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import ru.woodehy.amy.a4chanimage.exceptions.EmptyResponseBody;
import ru.woodehy.amy.a4chanimage.models.CatalogPage;
import ru.woodehy.amy.a4chanimage.models.ChanThread;
import ru.woodehy.amy.a4chanimage.models.ImageMetaObject;


public class FourChanClient {

    private final String defaultBoard = "wg";

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();

    /**
     * Method to retrieve from 4chan public api for catalogs and serialize them to array list
     * @return Array of image meta objects
     */
    public ArrayList<ImageMetaObject> getBoardThreads() throws IOException, EmptyResponseBody {
        Request request = new Request.Builder()
                .url(get_catalog_link(defaultBoard))
                .build();
        Call call = client.newCall(request);
        Log.d("ALL", "New call done. Waiting for getting the response");
        Response response = call.execute();
        Log.d("ALL", "Response successfully has been gotten");
        String response_body = response.body().string();

        Gson gson = new Gson();
        if(response_body.isEmpty()) {
            throw new EmptyResponseBody();
        }

        ArrayList<CatalogPage> catalogPages;
        CatalogPage[] catPages = gson.fromJson(response_body, CatalogPage[].class);
        catalogPages = new ArrayList<>(Arrays.asList(catPages));

        return extractOnlyRequired(catalogPages);
    }

    private ArrayList<ImageMetaObject> extractOnlyRequired(ArrayList<CatalogPage> pages) {
        LinkedList<ImageMetaObject> requiredData = new LinkedList<>();

        for (CatalogPage page : pages) {
            for (ChanThread thread : page.threads) {
                ImageMetaObject meta = new ImageMetaObject();

                meta.commentary = thread.com;
                meta.height = thread.h;
                meta.width = thread.w;
                meta.thumbnailHeight = thread.tn_h;
                meta.thumbnailWidth = thread.tn_w;
                meta.imageLink = get_full_image_link(thread.tim, thread.ext);
                meta.thumbnailLink = get_thumbnail_link(thread.tim);

                requiredData.add(meta);
            }
        }

        return new ArrayList<>(requiredData);
    }

    private String get_catalog_link(String board) {
        String catalogLinkTemplate = "http://a.4cdn.org/%s/catalog.json";
        return String.format(catalogLinkTemplate, board);
    }

    private String get_thumbnail_link(long tim) {
        return this.get_thumbnail_link(defaultBoard, tim);
    }

    private String get_thumbnail_link(String board, long tim) {
        String thumbnailImageLinkTemplate = "http://t.4cdn.org/%s/%d.jpg";
        return String.format(thumbnailImageLinkTemplate, board, tim);
    }

    private String get_full_image_link(long tim, String ext) {
        return this.get_full_image_link(defaultBoard, tim, ext);
    }

    private String get_full_image_link(String board, long tim, String ext) {
        String fullImageLinkTemplate = "http://i.4cdn.org/%s/%d%s";
        return String.format(fullImageLinkTemplate, board, tim, ext);
    }
}
