package ru.woodehy.amy.a4chanimage.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ru.woodehy.amy.a4chanimage.R;
import ru.woodehy.amy.a4chanimage.fragments.CatalogFragment;
import ru.woodehy.amy.a4chanimage.fragments.FullImageViewFragment;
import ru.woodehy.amy.a4chanimage.models.ImageMetaObject;

public class MainActivity extends AppCompatActivity implements CatalogFragment.OnListFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_main);
        if(savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.root_layout, CatalogFragment.newInstance(1), "CatalogFragment")
                    .commit();
        }
    }

    @Override
    public void onListFragmentInteraction(ImageMetaObject item) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.root_layout, FullImageViewFragment.newInstance(item), "FullImage");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack("FullImage");
        ft.commit();
    }
}
