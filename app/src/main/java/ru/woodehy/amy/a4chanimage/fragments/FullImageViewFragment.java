package ru.woodehy.amy.a4chanimage.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import ru.woodehy.amy.a4chanimage.R;
import ru.woodehy.amy.a4chanimage.models.ImageMetaObject;


public class FullImageViewFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    // private static final String ARG_PARAM1 = "param1";
    // private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
    private ImageMetaObject item;

    public FullImageViewFragment() {
        // Required empty public constructor
    }

    public static FullImageViewFragment newInstance(ImageMetaObject item) {
        FullImageViewFragment fragment = new FullImageViewFragment();
        Bundle args = new Bundle();
        args.putSerializable("item", item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_full_image_view, container, false);
        if (getArguments() != null) {
            item = (ImageMetaObject) getArguments().get("item");
            ImageView imageView = view.findViewById(R.id.full_image);
            if(imageView == null) {
                Log.d("Error", "There is no image view with this id. Something got wrong");
            }
            else {
                Log.d("MainActivity", String.format("given link: %s", item.imageLink));
                Glide.with(this)
                        .load(item.imageLink)
                        .into(imageView);
            }
        }
        view.bringToFront();
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
