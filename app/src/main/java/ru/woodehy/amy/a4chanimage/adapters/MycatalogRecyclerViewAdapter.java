package ru.woodehy.amy.a4chanimage.adapters;

import java.util.LinkedList;
import java.util.List;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import ru.woodehy.amy.a4chanimage.R;
import ru.woodehy.amy.a4chanimage.fragments.CatalogFragment;
import ru.woodehy.amy.a4chanimage.fragments.CatalogFragment.OnListFragmentInteractionListener;
import ru.woodehy.amy.a4chanimage.models.ImageMetaObject;


public class MycatalogRecyclerViewAdapter extends RecyclerView.Adapter<MycatalogRecyclerViewAdapter.ViewHolder> {

    private List<ImageMetaObject> mValues;
    private final OnListFragmentInteractionListener mListener;
    private CatalogFragment.OnLoadMoreListener onLoadMoreListener;

    public MycatalogRecyclerViewAdapter(List<ImageMetaObject> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_catalog, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mCommentary.setText(mValues.get(position).commentary);

        Glide.with(holder.mView)
                .load(mValues.get(position).thumbnailLink)
                .into(holder.mThumbnail);
//                .onLoadFailed(holder.mView.getResources().getDrawable(R.drawable.ic_cloud_download));
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Click", "List-item clicked");
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    public void setOnLoadMoreListener(CatalogFragment.OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public int addNewItems(List<ImageMetaObject> items) {
        List<ImageMetaObject> newValues = new LinkedList<ImageMetaObject>(items);
        newValues.addAll(mValues);
        mValues = newValues;

        int itemCount = getItemCount();

        notifyItemInserted(itemCount - 1);
        notifyDataSetChanged();
        return itemCount;
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mThumbnail;
        public final TextView mCommentary;
        public ImageMetaObject mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mThumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            mCommentary = (TextView) view.findViewById(R.id.commentary);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mCommentary.getText() + "'";
        }
    }
}
