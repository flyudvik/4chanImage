package ru.woodehy.amy.a4chanimage.models;

import java.io.Serializable;


public class ImageMetaObject implements Serializable {
    public String imageLink;
    public String thumbnailLink;
    public Integer width;
    public Integer height;
    public Integer thumbnailWidth;
    public Integer thumbnailHeight;
    public String commentary;
}
